<?php


class Helper
{
    private $argv;

    /**
     * Helper constructor.
     * @param $argv
     */
    public function __construct($argv)
    {
        $this->argv = $argv;
    }

    /**
     * return message error if the user enter less than or more than 4 params
     */
    public function validateArgvCount()
    {
        if (count($this->argv) !== 4) {
            print "please check number of the parameter passed by the command line";
            die();
        }
    }
}