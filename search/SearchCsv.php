<?php

include "./classes/AbstractSearch.php";

class SearchCsv extends AbstractSearch
{

    /**
     * @return mixed|void
     */
    public function searchFile()
    {
        try {
            $this->readCsvFile();
        } catch (Exception $exception) {
            print $exception->getMessage();
        }
    }

    /**
     * read csv file and search for specific record by providing key search
     */
    private function readCsvFile()
    {
        $handle = fopen($this->getFilePath(), 'rb');
        $result = '';
        while (($data = fgetcsv($handle, 1000, ",")) !== false) {

            if (!isset($data[$this->getSearchColumn()])) {
                print "specified column is out of range!\n";
                die();
            }
            if ($data[$this->getSearchColumn()] === $this->getSearchKey()) {
                $result .= implode(',', $data) . "\n";
            }
        }
        if ($result === '') {
            print "There is no data match the query";
            die();
        }
        print $result;
    }
}