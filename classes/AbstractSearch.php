<?php


abstract class AbstractSearch
{
    private $searchColumn;
    private $searchKey;
    private $filePath;


    /**
     * AbstractSearch constructor.
     * @param $filePath
     * @param $searchColumn
     * @param $searchKey
     */
    public function __construct($filePath, $searchColumn, $searchKey)
    {
        $this->setFilePath($filePath);
        $this->setSearchKey($searchKey);
        $this->setSearchColumn($searchColumn);
    }

    /**
     * @return mixed
     */
    public function getSearchColumn()
    {
        return $this->searchColumn;
    }

    /**
     * @return mixed
     */
    public function getSearchKey()
    {
        return $this->searchKey;
    }

    /**
     * @return mixed
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * @param $searchColumn
     */
    public function setSearchColumn($searchColumn)
    {
        if (!is_numeric($searchColumn)) {
            print "please enter a numeric value";
            die();
        }
        $this->searchColumn = (int)$searchColumn;
    }

    /**
     * @param $searchKey
     */
    public function setSearchKey($searchKey)
    {
        $this->searchKey = $searchKey;
    }


    /**
     * @param $filePath
     */
    public function setFilePath($filePath)
    {
        if (!file_exists($filePath)) {
            print "Please check file path and try again, file does not exists";
            die();
        }

        $this->filePath = $filePath;
    }

    /**
     * @return mixed
     */
    abstract public function searchFile();
}