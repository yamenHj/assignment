<?php

include "./search/SearchCsv.php";
include "./helper/Helper.php";

/**
 * first we will check if the number of params is 4 or not
 */
(new Helper($argv))->validateArgvCount();

/**
 * argv1 is file path
 * argv2 is search column
 * argv3 is search key
 */

try {
    $searchMethod = new SearchCsv($argv[1], $argv[2], $argv[3]);

    $searchMethod->searchFile();

} catch (Exception $exception) {
    print $exception->getMessage();
    die();
}


